import ms from 'ms';
import memoizer from 'lru-memoizer';
import debug from 'debug';

export default function(client, { cacheMaxEntries = 5, cacheMaxAge = '10h' }) {
  const logger = debug('jwks');
  const getSigningKey = client.getSigningKey;

  logger(`Configured caching of signing keys. Max: ${cacheMaxEntries} / Age: ${ms(cacheMaxAge)}`);
  return memoizer({
    load: (kid, callback) => {
      getSigningKey(kid, (err, key) => {
        if (err) {
          return callback(err);
        }

        logger(`Caching signing key for '${kid}':`, key);
        return callback(null, key);
      });
    },
    hash: (kid) => kid,
    maxAge: ms(cacheMaxAge),
    max: cacheMaxEntries
  });
}