import JwksClient from './JwksClient.js';
import JwksError from './lib/errors/JwksError.js';
import JwksRateLimitError from './lib/errors/JwksRateLimitError.js';
import SigningKeyNotFoundError from './lib/errors/SigningKeyNotFoundError.js';
import ArgumentError from './lib/errors/ArgumentError.js';

export default (options) => {
  return new JwksClient(options);
};

export { ArgumentError, JwksError, JwksRateLimitError, SigningKeyNotFoundError };